﻿namespace Largestproductinagrid
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.InputTextbox = new System.Windows.Forms.RichTextBox();
            this.YRowsLabel = new System.Windows.Forms.Label();
            this.calculateButton = new System.Windows.Forms.Button();
            this.largestProductLabel = new System.Windows.Forms.Label();
            this.largestProductTextbox = new System.Windows.Forms.TextBox();
            this.LargestRowProduct = new System.Windows.Forms.Label();
            this.LargestRowProductTextbox = new System.Windows.Forms.TextBox();
            this.LargestColumnLabel = new System.Windows.Forms.Label();
            this.LargestColumnProductTextbox = new System.Windows.Forms.TextBox();
            this.LargestDiagonalLeftProductLabel = new System.Windows.Forms.Label();
            this.LargestDiagonalLeftProductTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.LargestDiagonalRightProductTextbox = new System.Windows.Forms.TextBox();
            this.RandomizeGridButton = new System.Windows.Forms.Button();
            this.GridSize = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.GridSize)).BeginInit();
            this.SuspendLayout();
            // 
            // InputTextbox
            // 
            this.InputTextbox.Location = new System.Drawing.Point(13, 13);
            this.InputTextbox.Name = "InputTextbox";
            this.InputTextbox.Size = new System.Drawing.Size(321, 282);
            this.InputTextbox.TabIndex = 0;
            this.InputTextbox.Text = resources.GetString("InputTextbox.Text");
            // 
            // YRowsLabel
            // 
            this.YRowsLabel.AutoSize = true;
            this.YRowsLabel.Location = new System.Drawing.Point(12, 285);
            this.YRowsLabel.Name = "YRowsLabel";
            this.YRowsLabel.Size = new System.Drawing.Size(176, 13);
            this.YRowsLabel.TabIndex = 3;
            this.YRowsLabel.Text = "Size (X and Y needs to be the same";
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(12, 327);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(75, 23);
            this.calculateButton.TabIndex = 5;
            this.calculateButton.Text = "Calculate";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
            // 
            // largestProductLabel
            // 
            this.largestProductLabel.AutoSize = true;
            this.largestProductLabel.Location = new System.Drawing.Point(12, 371);
            this.largestProductLabel.Name = "largestProductLabel";
            this.largestProductLabel.Size = new System.Drawing.Size(84, 13);
            this.largestProductLabel.TabIndex = 6;
            this.largestProductLabel.Text = "Largest product:";
            // 
            // largestProductTextbox
            // 
            this.largestProductTextbox.Location = new System.Drawing.Point(12, 387);
            this.largestProductTextbox.Name = "largestProductTextbox";
            this.largestProductTextbox.Size = new System.Drawing.Size(100, 20);
            this.largestProductTextbox.TabIndex = 7;
            // 
            // LargestRowProduct
            // 
            this.LargestRowProduct.AutoSize = true;
            this.LargestRowProduct.Location = new System.Drawing.Point(131, 371);
            this.LargestRowProduct.Name = "LargestRowProduct";
            this.LargestRowProduct.Size = new System.Drawing.Size(104, 13);
            this.LargestRowProduct.TabIndex = 8;
            this.LargestRowProduct.Text = "Largest row product:";
            // 
            // LargestRowProductTextbox
            // 
            this.LargestRowProductTextbox.Location = new System.Drawing.Point(136, 387);
            this.LargestRowProductTextbox.Name = "LargestRowProductTextbox";
            this.LargestRowProductTextbox.Size = new System.Drawing.Size(100, 20);
            this.LargestRowProductTextbox.TabIndex = 9;
            // 
            // LargestColumnLabel
            // 
            this.LargestColumnLabel.AutoSize = true;
            this.LargestColumnLabel.Location = new System.Drawing.Point(9, 410);
            this.LargestColumnLabel.Name = "LargestColumnLabel";
            this.LargestColumnLabel.Size = new System.Drawing.Size(121, 13);
            this.LargestColumnLabel.TabIndex = 10;
            this.LargestColumnLabel.Text = "Largest column product:";
            // 
            // LargestColumnProductTextbox
            // 
            this.LargestColumnProductTextbox.Location = new System.Drawing.Point(12, 426);
            this.LargestColumnProductTextbox.Name = "LargestColumnProductTextbox";
            this.LargestColumnProductTextbox.Size = new System.Drawing.Size(100, 20);
            this.LargestColumnProductTextbox.TabIndex = 11;
            // 
            // LargestDiagonalLeftProductLabel
            // 
            this.LargestDiagonalLeftProductLabel.AutoSize = true;
            this.LargestDiagonalLeftProductLabel.Location = new System.Drawing.Point(9, 488);
            this.LargestDiagonalLeftProductLabel.Name = "LargestDiagonalLeftProductLabel";
            this.LargestDiagonalLeftProductLabel.Size = new System.Drawing.Size(146, 13);
            this.LargestDiagonalLeftProductLabel.TabIndex = 12;
            this.LargestDiagonalLeftProductLabel.Text = "Largest Diagonal left product:";
            // 
            // LargestDiagonalLeftProductTextbox
            // 
            this.LargestDiagonalLeftProductTextbox.Location = new System.Drawing.Point(12, 504);
            this.LargestDiagonalLeftProductTextbox.Name = "LargestDiagonalLeftProductTextbox";
            this.LargestDiagonalLeftProductTextbox.Size = new System.Drawing.Size(100, 20);
            this.LargestDiagonalLeftProductTextbox.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 449);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Largest diagonal right product:";
            // 
            // LargestDiagonalRightProductTextbox
            // 
            this.LargestDiagonalRightProductTextbox.Location = new System.Drawing.Point(12, 465);
            this.LargestDiagonalRightProductTextbox.Name = "LargestDiagonalRightProductTextbox";
            this.LargestDiagonalRightProductTextbox.Size = new System.Drawing.Size(100, 20);
            this.LargestDiagonalRightProductTextbox.TabIndex = 16;
            // 
            // RandomizeGridButton
            // 
            this.RandomizeGridButton.Location = new System.Drawing.Point(135, 327);
            this.RandomizeGridButton.Name = "RandomizeGridButton";
            this.RandomizeGridButton.Size = new System.Drawing.Size(100, 23);
            this.RandomizeGridButton.TabIndex = 17;
            this.RandomizeGridButton.Text = "Randomize grid";
            this.RandomizeGridButton.UseVisualStyleBackColor = true;
            this.RandomizeGridButton.Click += new System.EventHandler(this.RandomizeGridButton_Click);
            // 
            // GridSize
            // 
            this.GridSize.Increment = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.GridSize.Location = new System.Drawing.Point(13, 302);
            this.GridSize.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.GridSize.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.GridSize.Name = "GridSize";
            this.GridSize.Size = new System.Drawing.Size(99, 20);
            this.GridSize.TabIndex = 18;
            this.GridSize.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(367, 588);
            this.Controls.Add(this.GridSize);
            this.Controls.Add(this.RandomizeGridButton);
            this.Controls.Add(this.LargestDiagonalRightProductTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LargestDiagonalLeftProductTextbox);
            this.Controls.Add(this.LargestDiagonalLeftProductLabel);
            this.Controls.Add(this.LargestColumnProductTextbox);
            this.Controls.Add(this.LargestColumnLabel);
            this.Controls.Add(this.LargestRowProductTextbox);
            this.Controls.Add(this.LargestRowProduct);
            this.Controls.Add(this.largestProductTextbox);
            this.Controls.Add(this.largestProductLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.YRowsLabel);
            this.Controls.Add(this.InputTextbox);
            this.Name = "Form1";
            this.Text = "Largest product in a grid";
            ((System.ComponentModel.ISupportInitialize)(this.GridSize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox InputTextbox;
        private System.Windows.Forms.Label YRowsLabel;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label largestProductLabel;
        private System.Windows.Forms.TextBox largestProductTextbox;
        private System.Windows.Forms.Label LargestRowProduct;
        private System.Windows.Forms.TextBox LargestRowProductTextbox;
        private System.Windows.Forms.Label LargestColumnLabel;
        private System.Windows.Forms.TextBox LargestColumnProductTextbox;
        private System.Windows.Forms.Label LargestDiagonalLeftProductLabel;
        private System.Windows.Forms.TextBox LargestDiagonalLeftProductTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox LargestDiagonalRightProductTextbox;
        private System.Windows.Forms.Button RandomizeGridButton;
        private System.Windows.Forms.NumericUpDown GridSize;
    }
}

