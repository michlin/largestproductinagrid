﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Largestproductinagrid
{
    /// <summary>
    /// Class to handle the GUI
    /// </summary>
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }
        /// <summary>
        /// Handles when someone is clicking the Calculate-button.
        /// </summary>
        /// <param name="sender">A sender object</param>
        /// <param name="e">EventArgs</param>        
        private void calculateButton_Click(object sender, EventArgs e)
        {
            Largestproductinagrid.Calculate calc = new Largestproductinagrid.Calculate();
            int[,] grid = calc.constructGrid(InputTextbox.Text, Convert.ToInt32(GridSize.Text));
            int[] products = { calc.calculateLargestRowProduct(grid), calc.calculateLargestColumnProduct(grid), calc.calculateLargestDiagonalLeftProduct(grid), calc.calculateLargestDiagonalRightProduct(grid) };
            largestProductTextbox.Text = products.Max().ToString();
            LargestRowProductTextbox.Text = calc.calculateLargestRowProduct(grid).ToString();
            LargestColumnProductTextbox.Text = calc.calculateLargestColumnProduct(grid).ToString();
            LargestDiagonalRightProductTextbox.Text = calc.calculateLargestDiagonalRightProduct(grid).ToString();
            LargestDiagonalLeftProductTextbox.Text = calc.calculateLargestDiagonalLeftProduct(grid).ToString();
        }
        /// <summary>
        /// Handles the clickevent of randomize a grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RandomizeGridButton_Click(object sender, EventArgs e)
        {
            Largestproductinagrid.Calculate calc = new Largestproductinagrid.Calculate();
            InputTextbox.Text = convertGridToString(calc.randomizeGrid(Convert.ToInt32(GridSize.Text)));
        }
        /// <summary>
        /// Converts a given two dimensional array to string.
        /// </summary>
        /// <param name="grid">A given two dimensional array.</param>
        /// <returns>A string of a given two dimensional array.</returns>
        private string convertGridToString(int[,] grid)
        {
            String result = "";
            for(int i = 0; i < grid.GetLength(0); i++)
            {
                for(int j = 0; j < grid.GetLength(1); j++)
                {
                    if(j > 0)
                    { 
                        if (grid[i, j] < 10) //Adding the right format to the string.
                            result = result + ' ' + "0" + grid[i, j].ToString();
                        else
                            result = result + ' ' + grid[i, j].ToString();
                    }
                    else if (j <= 0)
                    {
                        if(grid[i,j] < 10)
                          result = result + "0" + grid[i, j].ToString();
                        else
                          result = result + grid[i, j].ToString();
                    }
                    else
                    {
                        result = result + ' ' + grid[i, j].ToString();
                    }
                }
                result = result + Environment.NewLine;
            }
            return result;
        }
    }
}
