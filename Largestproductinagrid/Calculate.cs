﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Largestproductinagrid
{
    /// <summary>
    /// Class Calculate handles the calculations
    /// </summary>
    public class Calculate
    {
        public int[,] randomizeGrid(int size)
        {
            int[,] randomizedGrid = new int[size, size];
            Random randomizedCell = new Random();
                
            for(int i = 0; i < size; i++)
            {
                for(int j = 0; j < size; j++)
                {
                    randomizedGrid[i, j] = randomizedCell.Next(1,100);
                }
            }
            return randomizedGrid;
        }
        /// <summary>
        /// constructGrid takes the input text from the gui and converts it to a two dimensional 
        /// </summary>
        /// <param name="inputText"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int[,] constructGrid(string inputText, int size)
        {
            int[,] grid = new int[size, size];
            string[] rows = inputText.Split(Environment.NewLine.ToArray(),StringSplitOptions.RemoveEmptyEntries);
            if(size > rows.Length)
            {
                MessageBox.Show("Error, the input matrix does not correspond to the size that was entered. Try again.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return new int[0, 0];
            }
            for (int i = 0; i < size; i++)
            {
                int[] columnsinrow = Array.ConvertAll(rows[i].Split(' '), int.Parse);
                for (int j = 0; j < size; j++)
                {
                    grid[i, j] = columnsinrow[j];
                }
            }
            return grid;
        }
        /// <summary>
        /// calculateLargestRowProduct method to calculate the largest product of four cells on the same row in a given grid.
        /// </summary>
        /// <param name="grid">A given grid as a two dimensional array.</param>
        /// <returns>Returns the largest product of four cell on the same row as an integer.</returns>
        public int calculateLargestRowProduct(int[,] grid)
        {
            int product, biggestRowProduct = 0;
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int row = 0; row < grid.GetLength(1) - 4; row++)
                {
                    product = grid[i, row] * grid[i, row + 1] * grid[i, row + 2] * grid[i, row + 3];
                    if (product > biggestRowProduct)
                        biggestRowProduct = product;
                }
            }
            return biggestRowProduct;
        }
        /// <summary>
        /// calculateLargestColumnProduct method to calculate the largest column product of four cells following each other in a given grid.
        /// </summary>
        /// <param name="grid">A given grid as a two dimensional array.</param>
        /// <returns>Returns the largest product of four columns as an integer. </returns>
        public int calculateLargestColumnProduct(int[,] grid)
        {
            int product, biggestColumnProduct = 0;
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int column = 0; column < grid.GetLength(1) - 4; column++)
                {
                    product = grid[column, i] * grid[column + 1, i] * grid[column + 2, i] * grid[column + 3, i];
                    if (product > biggestColumnProduct)
                        biggestColumnProduct = product;
                }
            }
            return biggestColumnProduct;
        }
        /// <summary>
        /// calculateLargestDiagonalRightProduct method to calculate the largest Diagonal product. The direction of the diagonal is right.
        /// </summary>
        /// <param name="grid">A given grid as a two dimensional array.</param>
        /// <returns>Returns the largest product of four cells in a diagonal from right.</returns>
        public int calculateLargestDiagonalRightProduct(int[,] grid)
        {
            int product, biggestDiagonalRightProduct = 0;
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                if(i < grid.GetLength(0)-4)
                {
                    for (int diagonalRight = 0; diagonalRight < grid.GetLength(1) - 4; diagonalRight++)
                    {
                        product = grid[i, diagonalRight] * grid[i + 1, diagonalRight + 1] * grid[i + 2, diagonalRight + 2] * grid[i + 3, diagonalRight + 3];
                        if (product > biggestDiagonalRightProduct)
                            biggestDiagonalRightProduct = product;
                    }
                }
            }
            return biggestDiagonalRightProduct;
        }
        /// <summary>
        /// calculateLargestDiagonalLeftProduct method to calculate the largest diagonal product. The direction of the diagonal is left.
        /// </summary>
        /// <param name="grid">A given grid as a two dimensional array.</param>
        /// <returns>Returns the largest product of four cells in a diagonal from left.</returns>
        public int calculateLargestDiagonalLeftProduct(int[,] grid)
        {
            int product, biggestDiagonalRightProduct = 0;
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                if (i < grid.GetLength(0) - 4)
                {
                    for (int diabognalLeft = grid.GetLength(1) - 1; diabognalLeft > 4; diabognalLeft--)
                    {
                        product = grid[diabognalLeft,i] * grid[diabognalLeft - 1, i + 1] * grid[diabognalLeft - 2, i + 2] * grid[diabognalLeft - 3, i + 3];
                        if (product > biggestDiagonalRightProduct)
                            biggestDiagonalRightProduct = product;
                    }
                }
            }
            return biggestDiagonalRightProduct;
        }
    }
}
